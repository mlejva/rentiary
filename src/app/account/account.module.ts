import { NgModule }     from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AccountComponent }     from './account.component';
import { AccountInfoComponent}  from './account-info.component';

import { AccountRouting }   from './account.routing';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        AccountRouting
    ],
    declarations: [
        AccountComponent,
        AccountInfoComponent
    ]
})
export class AccountModule { }
