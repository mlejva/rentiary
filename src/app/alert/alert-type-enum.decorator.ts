import { AlertTypeEnum } from './alert';

export function AlertTypeEnumAware(constructor: Function) {
    constructor.prototype.AlertTypeEnum = AlertTypeEnum;
}