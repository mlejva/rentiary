export enum AlertTypeEnum {
    error,
    success
}

export class Alert {
    private _type: AlertTypeEnum
    private _text: string

    get type(): AlertTypeEnum {
        return this._type;
    }

    get text(): string {
        return this._text;
    }

    constructor(type: AlertTypeEnum, text: string) {
        this._type = type;
        this._text = text;
    }
}