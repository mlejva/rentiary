import { Component } from '@angular/core';
import { Router }    from '@angular/router';
import { Location }  from '@angular/common';

import { UserService }  from '../user/user.service';
import { AlertService } from '../alert/alert.service';

import { Alert, AlertTypeEnum } from '../alert/alert';

@Component({
    templateUrl: 'login.component.html',
    styleUrls: [ 'login.component.css' ]
})

export class LoginComponent {
    // TODO: http://jasonwatmore.com/post/2016/12/08/angular-2-redirect-to-previous-url-after-login-with-auth-guard

    email = '';
    password = '';

    loading = false;

    constructor(
        private router: Router,
        private location: Location,
        private userService: UserService,
        private alertService: AlertService) { }

    goBack(): void {
        this.location.back();
    }

    loginUser(): void {
        this.loading = true;

        this.userService.login(this.email, this.password)
            .then((user) => {
                this.loading = false;
                this.router.navigate(['/home']);
            })
            .catch((error: any) => {
                this.loading = false;
                this.handleLoginError(error);
            });
    }

    handleLoginError(error: any): void {
        // Firebase bug - https://github.com/angular/angularfire2/issues/976
        const errorCode: string = error.code;
        let errorMessage: string;

        if (errorCode == 'auth/invalid-email') {
            errorMessage = 'Entered email is invalid';
        }
        else if (errorCode == 'auth/user-disabled') {
            errorMessage = 'Account associated with this email was disabled';
        }
        else if (errorCode == 'auth/user-not-found') {
            errorMessage = `Account with this email address or password doesn't exist`;
        }
        else if (errorCode == 'auth/wrong-password') {
            errorMessage = `Account with this email address or password doesn't exist`;
        }

        const alert = new Alert(AlertTypeEnum.error, errorMessage);
        this.alertService.alert(alert);
    }
}
