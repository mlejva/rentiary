import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import * as firebase  from 'firebase/app';

import { UserService } from '../user/user.service';

@Component({
    selector: 'navbar',
    templateUrl: 'navbar.component.html',
    styleUrls: [ 'navbar.component.css' ]
})

export class NavbarComponent implements OnInit {
    showBurgerMenu = false;

    constructor(
        private userService: UserService,
        private router: Router) { }

    ngOnInit() {
    }

    toggleBurgerMenu(): void { this.showBurgerMenu = !this.showBurgerMenu; }
    logout(): void {
        // TODO: Should handle promise?
        // TODO: Should show loader?
        this.userService.logout()
            .then(() => this.router.navigate(['/home']));

    }
}