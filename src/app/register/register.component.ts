import { Component } from '@angular/core';
import { Router }    from '@angular/router';

import { UserService }  from '../user/user.service';
import { AlertService } from '../alert/alert.service';

import { Alert, AlertTypeEnum } from '../alert/alert';

@Component({
    templateUrl: 'register.component.html',
    styleUrls: [ 'register.component.css' ]
})

export class RegisterComponent {
    showRegisterForm = false;

    username = '';
    email = '';
    password = '';
    passwordCheck = '';

    loading = false;

    constructor(
        private router: Router,
        private userService: UserService,
        private alertService: AlertService) { }

    registerUser(): void {
        this.loading = true;

        this.userService.register(this.username, this.email, this.password)
            .then(() => {
                this.loading = false;
                this.router.navigate(['/home']);
            })
            .catch((error: any) => {
                this.loading = false;
                this.handleRegisterError(error);
            });
    }

    handleRegisterError(error: any): void {
        // Firebase bug - https://github.com/angular/angularfire2/issues/976
        const errorCode: string = error.code;
        let errorMessage: string;

        if (errorCode == 'auth/email-already-in-use') {
            errorMessage = 'This email is already registered';
        }
        else if (errorCode == 'auth/invalid-email') {
            errorMessage = 'Entered email is invalid';
        }
        else if (errorCode == 'auth/weak-password') {
            errorMessage = 'Please choose a password with at least 6 characters'
        }

        const alert = new Alert(AlertTypeEnum.error, errorMessage);
        this.alertService.alert(alert);
    }
}
