import { environment }    from '../../environments/environment';

import { NgModule }     from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }  from '@angular/forms';

import { FirebaseUIModule } from 'firebaseui-angular';
import { RegisterRouting }  from './register.routing';
import { AlertModule }      from '../alert/alert.module';

import { RegisterComponent } from './register.component';

import { UserService }  from '../user/user.service';
import { AlertService } from '../alert/alert.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RegisterRouting,
        AlertModule,
        FirebaseUIModule
    ],
    declarations: [
        RegisterComponent
    ],
    exports: [ RegisterComponent ],
})
export class RegisterModule { }
