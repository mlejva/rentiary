// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

import { AuthProvider, AuthMethods } from 'firebaseui-angular';

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAtXlESbwKHnyGvceljqxI7moJ_ztEqIgI",
    authDomain: "rentiary.firebaseapp.com",
    databaseURL: "https://rentiary.firebaseio.com",
    projectId: "rentiary",
    storageBucket: "rentiary.appspot.com",
    messagingSenderId: "116345320865"
  },
  firebaseUiConfig: {
    providers: [
      AuthProvider.Google,
      AuthProvider.Facebook,
      AuthProvider.Twitter
    ],
    method: AuthMethods.Popup,
    tos: '<your-tos-link>'
  }
};
